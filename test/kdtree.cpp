
#include <catch.hpp>

#include <mann.h>

#include "./test_input.h"

TEST_CASE("KDTree", "[KDTree]") {
  auto points = test::Repeated::points();
  auto tree = mann::BuildKDTree(points.begin(), points.end());

  CHECK(std::distance(points.begin(), points.end()) ==
        std::distance(tree.first_point(), tree.last_point()));
}

TEST_CASE("Norms", "[KDTree]") {
  using mann::Norm;

  static_assert(Norm<double>::Pow(-2.0) == 2.0, "L_p default error");
  static_assert(Norm<double, 1>::Pow(-2.0) == 2.0, "L_p p=1 error");
  static_assert(Norm<double, 3>::Pow(2.0) == 8.0, "L_p p=2 error");
  static_assert(Norm<double, 3>::Pow(-2.0) == 8.0, "L_p p=3 error");

  CHECK((Norm<double, 3, 2>::Pow(-2.0)) == Approx(std::pow(2.0, 3.0 / 2.0)));
}
